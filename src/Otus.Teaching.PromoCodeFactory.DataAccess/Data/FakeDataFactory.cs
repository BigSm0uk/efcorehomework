﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                AppliedPromocodesCount = 5,
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 10,
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };
        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
                {
                    new PromoCode()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99e"),
                        Code = "235311241245",
                        //ServiceInfo = "Товары для дома",
                        BeginDate = DateTime.ParseExact("2020-03-21 13:26", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        EndDate = DateTime.ParseExact("2022-03-21 13:26", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        PartnerName = "Николай",
                        EmployeeId = Employees.FirstOrDefault(x=> x.FirstName=="Петр").Id,
                        PreferenceId = Preferences.FirstOrDefault(x=> x.Name=="Семья").Id,
                    },
                    new PromoCode()
                    {
                        Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99d"),
                        Code = "235315",
                        //ServiceInfo = "Товары для машины",
                        BeginDate = DateTime.ParseExact("2020-03-21 13:26", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        EndDate = DateTime.ParseExact("2022-03-21 13:26", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                        PartnerName = "Женя",
                        EmployeeId = Employees.FirstOrDefault(x=> x.FirstName=="Иван").Id,
                        PreferenceId = Preferences.FirstOrDefault(x=> x.Name=="Дети").Id,
                    }
                };

        public static IEnumerable<CustomerPreference> CustomersPreferences => new List<CustomerPreference>()
        {
            new CustomerPreference
            {
                PreferenceId = Preferences.FirstOrDefault(x=>x.Name=="Дети").Id,
                CustomerId = Customers.FirstOrDefault(x=>x.FirstName=="Иван").Id,
            }
        };
        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        //TODO: Добавить предзаполненный список предпочтений
                        //Preferences = new List<Preference>(){ Preferences.FirstOrDefault(x=>x.Name == "Театр")},
                        PromoCodesId = PromoCodes.FirstOrDefault(x=>x.PartnerName=="Женя").Id
                    }
                };

                return customers;
            }
        }
    }
}