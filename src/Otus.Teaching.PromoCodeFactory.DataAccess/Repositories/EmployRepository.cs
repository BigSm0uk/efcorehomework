﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployRepository : EfRepository<Employee>
    {
        public EmployRepository(DataContex dataContex) : base(dataContex)
        {
        }
        public async Task<Employee> GetByIdWithRoleAsync(Guid id)
        {
            return await Task.FromResult(_dataContext.Employees.FirstOrDefault(x=> x.Id==id)!);
        }
    }
}