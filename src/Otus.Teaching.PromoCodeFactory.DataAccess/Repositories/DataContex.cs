﻿using System;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class DataContex : DbContext
    {
        public DbSet<Employee> Employees { get; set; } = null!;
        public DbSet<Role> Roles { get; set; } = null!;
        public DbSet<PromoCode> PromoCodes { get; set; } = null!;
        public DbSet<Customer> Customers { get; set; } = null!;
        public DbSet<Preference> Preferences { get; set; } = null!;
        
        //public DbSet<CustomerPreference> CustomerPreferences { get; set; } = null!;
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlite(x=> x.MigrationsAssembly("Otus.Teaching.PromoCodeFactory.WebHost"));
        }
        public DataContex(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Employee>().HasKey(u => u.Id);
            
            
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Role>().HasKey(u => u.Id);
            
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Preference>().HasKey(u => u.Id);
            
            modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes);
            modelBuilder.Entity<PromoCode>().HasKey(u => u.Id);
            
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<Customer>().HasKey(u => u.Id);
            modelBuilder.Entity<Customer>().HasOne(u => u.PromoCodes).WithMany(k => k.Customer).HasForeignKey(k=>k.PromoCodesId);
            modelBuilder.Entity<Customer>().
                HasMany(u => u.Preferences).
                WithMany(u => u.Customers)
                .UsingEntity<CustomerPreference>(
                    j=> j.HasOne(pt=>pt.Preference).
                        WithMany(pt=>pt.CustomerPreferences).
                        HasForeignKey(u=>u.PreferenceId),
                    j=>j.HasOne(pt=>pt.Customer).
                        WithMany(t=>t.CustomerPreferences).
                        HasForeignKey(u=>u.CustomerId),
                    j =>
                    {
                        j.HasKey(t => new { t.CustomerId, t.PreferenceId });
                        j.ToTable("CustomerPreference");
                        
                    });
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomersPreferences);
            base.OnModelCreating(modelBuilder);
        }
    }
}