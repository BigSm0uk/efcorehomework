﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>
    {
        public CustomerRepository(DataContex dataContex) : base(dataContex)
        { }
        public async Task<Customer> GetByIdAsync(Guid id)
        {
            return await Task.FromResult(_dataContext.Customers.FirstOrDefault(x=> x.Id==id)!);
        }

        public async Task<Customer> UpdateByIdAsync(Guid id, Customer customer)
        {
            await _dataContext.SaveChangesAsync();
            return customer;
        }
        public async Task<Customer> CreateAsync(Customer item)
        {
            _dataContext.Customers.Add(item);
            await _dataContext.SaveChangesAsync();
            return item;
        }
        public async Task<Customer> DeleteItem(Customer item)
        {
            _dataContext.Customers.Remove(item);
            await _dataContext.SaveChangesAsync();
            return item;
        }
        public async Task<IEnumerable<Customer>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await Task.FromResult<IEnumerable<Customer>>(ids.Select(variable => GetByIdAsync(variable).Result).ToList());
        }
    }
}