﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected readonly DataContex _dataContext;
        
        public EfRepository(DataContex dataContex)
        {
            _dataContext = dataContex;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await Task.FromResult(_dataContext.Set<T>().FirstOrDefault(x=> x.Id==id)!);

        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> toList)
        {
            throw new NotImplementedException();
        }

        public Task<T> UpdateByIdAsync(Guid id, T toUpdate)
        {
            throw new NotImplementedException();
        }

        public Task<T> CreateAsync(T item)
        {
            throw new NotImplementedException();
        }

        public Task<T> DeleteItem(T item)
        {
            throw new NotImplementedException();
        }
    }
}