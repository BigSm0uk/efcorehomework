﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        //[MaxLength(30)]
        public string Code { get; set; }
        //[MaxLength(30)]
        //public string ServiceInfo { get; set; }
        
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        //[MaxLength(32)]
        public string PartnerName { get; set; }

        virtual public Employee PartnerManager { get; set; }
        
        public Guid EmployeeId { get; set; }

        virtual public Preference Preference { get; set; }
        
        public Guid PreferenceId { get; set; }
        
        virtual public List<Customer> Customer { get; set; }
    }
}