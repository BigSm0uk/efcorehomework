﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {   
        //[MaxLength(32)]
        public string FirstName { get; set; }
        //[MaxLength(32)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        virtual public List<Preference> Preferences { get; set; }
        
        virtual public List<CustomerPreference> CustomerPreferences { get; set; }

        virtual public PromoCode PromoCodes { get; set; }

        public Guid PromoCodesId { get; set; }
        
        
    }
}