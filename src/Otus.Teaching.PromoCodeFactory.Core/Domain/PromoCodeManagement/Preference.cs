﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        //[MaxLength(32)]
        public string Name { get; set; }

        public virtual List<Customer> Customers { get; set; }
        
        public virtual List<CustomerPreference> CustomerPreferences { get; set; }
        
    }
}