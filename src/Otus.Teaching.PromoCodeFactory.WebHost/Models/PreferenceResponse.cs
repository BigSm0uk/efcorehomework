﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        
        public static List<PreferenceResponse> PreferenceToResponse(List<Preference> preferences)
        {
            return preferences.Select(variable => new PreferenceResponse() { id = variable.Id, Name = variable.Name, }).ToList();
        }
    }
}