﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Migrations
{
    public partial class Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InfoAboutBeginDate",
                table: "PromoCodes");

            migrationBuilder.DropColumn(
                name: "InfoAboutPerson",
                table: "PromoCodes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InfoAboutBeginDate",
                table: "PromoCodes",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InfoAboutPerson",
                table: "PromoCodes",
                type: "TEXT",
                nullable: true);
        }
    }
}
