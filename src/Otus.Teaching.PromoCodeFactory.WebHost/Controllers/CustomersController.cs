﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly CustomerRepository _customerRepository;
        private readonly PreferenceRepository _preferenceRepository;
        private readonly PromoCodeRepository _promocodeRepository;

        public CustomersController(CustomerRepository customerRepository , PreferenceRepository preferenceRepository, PromoCodeRepository promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeRepository = promocodeRepository;
        }
        /// <summary>
        /// Получить данные всех покупателей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customer = await _customerRepository.GetAllAsync();
            var customerShort = customer.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                FirstName = x.FirstName,    
                LastName = x.LastName,
                Email = x.Email
            });

            return customerShort.ToList();
        }
        /// <summary>
        /// Получить данные покупателя по id 
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<CustomerResponse> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var customerShort = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCode = new PromoCodeShortResponse()
                {
                    Id = customer.PromoCodesId,
                    Code = customer.PromoCodes.Code,
                    //ServiceInfo = customer.PromoCodes.ServiceInfo,
                    BeginDate = customer.PromoCodes.BeginDate.ToString(),
                    EndDate = customer.PromoCodes.EndDate.ToString(),
                    PartnerName = customer.PromoCodes.PartnerName
                },
                PreferenceResponses = PreferenceResponse.PreferenceToResponse(customer.Preferences)
            };
            return customerShort;
        }
        /// <summary>
        /// Создать нового покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences =  await GetPreferencesAsync(request.PreferenceIds);
            var promocode = await GetPromocodeAsync(request.PromocodeId);
            var customer = new Customer();
            customer.Id = Guid.NewGuid();
            CustomerMapper(promocode, preferences.ToList(), request, customer);
            await _customerRepository.CreateAsync(customer);
            return Ok();
        }
        /// <summary>
        /// Обновить покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();
            var preferences =  await GetPreferencesAsync(request.PreferenceIds);
            var promocode = await GetPromocodeAsync(request.PromocodeId);
            CustomerMapper(promocode, preferences.ToList(), request, customer);
            await _customerRepository.UpdateByIdAsync(id, customer);
            return Ok();
        }
        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            await _customerRepository.DeleteItem(customer);
            return Ok();
        }
        
        private Task<IEnumerable<Preference>> GetPreferencesAsync(IEnumerable<Guid> ids)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            if (ids != null && ids.Any())
            {
                return _preferenceRepository
                    .GetRangeByIdsAsync(ids.ToList());
            }

            return Task.FromResult(preferences);
        }
        private Task<PromoCode> GetPromocodeAsync(Guid id)
        {
            PromoCode promoCode = new PromoCode();
            if (id != null)
            {
                return _promocodeRepository.GetByIdAsync(id);
            }
            return Task.FromResult(promoCode);
        }

        private void CustomerMapper(PromoCode promoCode, List<Preference> preference, CreateOrEditCustomerRequest customerRequest, Customer customer)
        {
            customer.Email = customerRequest.Email;
            customer.FirstName = customerRequest.FirstName;
            customer.LastName = customerRequest.LastName;
            customer.PromoCodes = promoCode;
            customer.PromoCodesId = customer.PromoCodes.Id;
            if (preference != null && preference.Any())
            {
                /*customer.CustomerPreferences?.Clear();
                customer.CustomerPreferences = preference.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    CustomerId = customer.Id,
                    PreferenceId = x.Id,
                    Preference = x
                }).ToList();*/
                customer.Preferences = preference;
                //customer.Preferences.Select(x => x.CustomerPreferences = customer.CustomerPreferences);
            }
            customer.PromoCodes.Customer.Add(customer);
            foreach (var variable in customer.Preferences)
            {
                variable.Customers.Add(customer);
            }
        }

    }
}